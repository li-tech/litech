 // mohtasham React layouts
import SignIn from "layouts/authentication/sign-in"; 

// @mui icons
import DashboardIcon from "@mui/icons-material/Dashboard"; 
import ExitToAppOutlinedIcon from "@mui/icons-material/ExitToAppOutlined"; 
import Dashboard from "layouts/dashboard";

import Category from "layouts/catalogue/category";
import Categories from "layouts/catalogue/categories";
import Catalogues from "layouts/catalogue/catalogues";
import Catalogue from "layouts/catalogue/catalogue";
import CatalogueView from "layouts/catalogue/catalogueView";
 
const routes = [
  {
    type: "collapse",
    name: "داشبورد",
    key: "dashboard",
    show: true,
    auth: true,
    icon: <DashboardIcon />,
    route: "/dashboard",
    component: <Dashboard />,
  }, 
  {
    type: "collapse",
    name: "دسته بندی کاتالوگ",
    key: "category",
    show: false,
    auth: true,
    icon: <DashboardIcon />,
    route: "/category",
    component: <Category />,
  },
  {
    type: "collapse",
    name: "دسته بندی کاتالوگ",
    key: "category",
    show: false,
    auth: true,
    icon: <DashboardIcon />,
    route: "/category/:id",
    component: <Category />,
  },
  {
    type: "collapse",
    name: "دسته بندی کاتالوگ",
    key: "categories",
    show: true,
    auth: true,
    icon: <DashboardIcon />,
    route: "/categories",
    component: <Categories />,
  },
  {
    type: "collapse",
    name: " مدیریت کاتالوگ",
    key: "Catalogues",
    show: true,
    auth: true,
    icon: <DashboardIcon />,
    route: "/Catalogues",
    component: <Catalogues />,
  },
  {
    type: "collapse",
    name: "کاتالوگ",
    key: "catalogue",
    show: false,
    auth: true,
    icon: <DashboardIcon />,
    route: "/catalogue/",
    component: <Catalogue />,
  },
  {
    type: "collapse",
    name: "کاتالوگ",
    key: "catalogue",
    show: false,
    auth: true,
    icon: <DashboardIcon />,
    route: "/catalogue/:id",
    component: <Catalogue />,
  },
  {
    type: "collapse",
    name: "کاتالوگ",
    key: "catalogueview",
    show: true,
    auth: true,
    icon: <DashboardIcon />,
    route: "/catalogueview",
    component: <CatalogueView />,
  },

  {
    type: "collapse",
    name: "خروج",
    key: "sign-out",
    auth: true,
    show: true,
    icon: <ExitToAppOutlinedIcon />,
    route: "/authentication/sign-in",
    component: <SignIn />,
  },
];

export default routes;
