/**
=========================================================
* mohtasham React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// prop-types is a library for typechecking of props
import PropTypes from "prop-types";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
// @mui material components
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined"; // mohtasham React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import { Grid } from "@mui/material";
import { useNavigate } from "react-router-dom";

function Footer({ links }) {
  const navigate = useNavigate();
  const renderLinks = () =>
    links.map((link) => (
      
      <MDBox key={link.name} px={3}>
        <Grid
          onClick={() => {
            navigate(link.href);
          }}
          style={{
            cursor: "pointer",
            flexDirection: "column",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {link.icon}
          <MDTypography
            style={{ color: "#FFF" }}
            variant="button"
            fontWeight="regular"
            color="#FFF"
          >
            {link.name}
          </MDTypography>
        </Grid>
      </MDBox>
    ));

  return (
    <div
      style={{
        height: 50,
        width: "100%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        backgroundColor: "#30BFC2",
        position: "fixed",
        left: 0,
        right: 0,
        bottom: 0,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
      }}
    >
      <MDBox
        width="100%"
        display="flex"
        flexDirection="column"
        justifyContent="space-between"
        alignItems="center"
        px={1.5}
      >
        <MDBox
          component="ul"
          style={{
            display: "flex",
            flexWrap: "wrap",
            alignItems: "center",
            justifyContent: "center",
            listStyle: "none",
            marginStart: "auto",
            marginEnd: "auto",
            backGrounColor: "red",
            mt: 3,
            mb: 0,
            p: 0,
          }}
        >
          {renderLinks()}
        </MDBox>
      </MDBox>
    </div>
  );
}

// Setting default values for the props of Footer
Footer.defaultProps = {
  links: [
    {
      href: "/dashboard",
      name: "home",
      icon: <HomeOutlinedIcon style={{ fill: "#FFF" }} />,
    },
    {
      href: "/profile",
      name: "profile",
      icon: <PersonOutlineOutlinedIcon style={{ fill: "#FFF" }} />,
    },
  ],
};

// Typechecking props for the Footer
Footer.propTypes = {
  links: PropTypes.arrayOf(PropTypes.object),
};

export default Footer;
