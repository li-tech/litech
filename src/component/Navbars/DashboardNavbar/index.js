import { useState, useEffect } from "react";

import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import MenuOpenOutlinedIcon from "@mui/icons-material/MenuOpenOutlined";
import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";
import MDBox from "components/MDBox";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
// import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import {
  navbar,
  navbarContainer,
  navbarRow,
  navbarMobileMenu,
} from "component/Navbars/DashboardNavbar/styles";

import { useMaterialUIController, setTransparentNavbar, setMiniSidenav } from "context";
import { useNavigate } from "react-router-dom";

function DashboardNavbar({ absolute, light, isMini, title }) {
  const [navbarType, setNavbarType] = useState();
  const [controller, dispatch] = useMaterialUIController();
  const { miniSidenav, transparentNavbar, fixedNavbar, darkMode } = controller;
  const navigate = useNavigate();
  useEffect(() => {
    if (fixedNavbar) {
      setNavbarType("sticky");
    } else {
      setNavbarType("static");
    }

    function handleTransparentNavbar() {
      setTransparentNavbar(dispatch, (fixedNavbar && window.scrollY === 0) || !fixedNavbar);
    }

    window.addEventListener("scroll", handleTransparentNavbar);

    handleTransparentNavbar();

    return () => window.removeEventListener("scroll", handleTransparentNavbar);
  }, [dispatch, fixedNavbar]);

  const handleMiniSidenav = () => setMiniSidenav(dispatch, !miniSidenav);
  // const handleConfiguratorOpen = () => setOpenConfigurator(dispatch, !openConfigurator);

  const iconsStyle = ({ palette: { dark, white, text }, functions: { rgba } }) => ({
    color: () => {
      let colorValue = light || darkMode ? white.main : dark.main;

      if (transparentNavbar && !light) {
        colorValue = darkMode ? rgba(text.main, -1) : text.main;
      }

      return colorValue;
    },
  });

  return (
    <AppBar
      position={absolute ? "absolute" : navbarType}
      color="inherit"
      sx={(theme) => navbar(theme, { transparentNavbar, absolute, light, darkMode })}
    >
      <Toolbar
        sx={(theme) => navbarContainer(theme)}
        style={{
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <MDBox
          style={{ alignItems: "center", justifyContent: "space-between !important" }}
          sx={(theme) => navbarRow(theme, { isMini })}
          md={(theme) => navbarRow(theme, { isMini })}
          lg={(theme) => navbarRow(theme, { isMini })}
        >
          <IconButton
            disableRipple
            color="inherit"
            sx={navbarMobileMenu}
            onClick={handleMiniSidenav}
          >
            {miniSidenav && <MenuOpenOutlinedIcon fontSize="medium" sx={iconsStyle} />}
            {!miniSidenav && <MenuOutlinedIcon fontSize="medium" sx={iconsStyle} />}
          </IconButton>
          <MDBox>{title}</MDBox>
          {window.history.state && window.history.state.idx > 0 && (
            <IconButton
              style={{ marginTop: -10 }}
              disableRipple
              color="inherit"
              sx={navbarMobileMenu}
              onClick={() => {
                navigate(-1);
              }}
            >
              <ArrowBackIcon />
            </IconButton>
          )}

          {/* <IconButton
                style={{ marginTop: -10 }}
                disableRipple
                color="inherit"
                sx={navbarMobileMenu}
                onClick={handleConfiguratorOpen}
              >
                <SettingsOutlinedIcon />
              </IconButton> */}
        </MDBox>
      </Toolbar>
    </AppBar>
  );
}

// Setting default values for the props of DashboardNavbar
DashboardNavbar.defaultProps = {
  title: "داشبورد",
  absolute: false,
  light: false,
  isMini: false,
};

// Typechecking props for the DashboardNavbar
DashboardNavbar.propTypes = {
  title: PropTypes.string,
  absolute: PropTypes.bool,
  light: PropTypes.bool,
  isMini: PropTypes.bool,
};

export default DashboardNavbar;
