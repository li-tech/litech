import { useEffect, useState } from "react";
// @mui material components
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import LoopIcon from "@mui/icons-material/Loop";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
// Acromegaly React components
import MDBox from "components/MDBox";
import Swal from "sweetalert2";
import LoadingOverlay from "react-loading-overlay";
// Acromegaly React example components
import DashboardLayout from "component/LayoutContainers/DashboardLayout";
import DashboardNavbar from "component/Navbars/DashboardNavbar";
import PropTypes from "prop-types";
// Acromegaly React contexts
import { useMaterialUIController, setDirection } from "context";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Collapse,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Tooltip,
} from "@mui/material";
import { getUsersList, deleteUser } from "controllers/UserController";
import MDButton from "components/MDButton";

function TablePaginationActions(props) {
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5, direction: "rtl" }}>
      <IconButton onClick={handleFirstPageButtonClick} disabled={page === 0} aria-label="صفحه اول">
        <LastPageIcon />
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        <KeyboardArrowRight />
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="اخرین صفحه"
      >
        <KeyboardArrowLeft />
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="صفحه قبلی"
      >
        <FirstPageIcon />
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const filters = {
  userRole: "",
};

function Tables() {
  const [controller, dispatch] = useMaterialUIController();
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(0);
  const [filter, setFilter] = useState(filters);
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(0);
  const [open, setOpen] = useState();
  const [datas, setDatas] = useState([]);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const navigate = useNavigate();
  let emptyRows = rowsPerPage - Math.min(rowsPerPage, count - page * rowsPerPage);

  // Avoid a layout jump when reaching the last page with empty rows.
  const getData = async (perPagex, pagex, filterx, searchx) => {
    setLoading(true);
    setDatas([]);
    const data = await getUsersList(perPagex, pagex, searchx, filterx, controller.token);
    if (data.success) {
      setDatas(data.data);

      setCount(data.count);
      emptyRows = rowsPerPage - Math.min(rowsPerPage, datas.length - (pagex - 1) * rowsPerPage);
    } else {
      Swal.fire({
        text: data.message,
        icon: "error",
        confirmButtonText: "بسیار خوب",
      });

      if (data.code === 401) {
        navigate("/", { replace: true });
      }
    }

    setLoading(false);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    getData(rowsPerPage, newPage + 1, filter, search);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setFilter("");
    getData(parseInt(event.target.value, 10), 1, filter, search);
  };
  const handleDeleteClick = async (id) => {
    Swal.fire({
      title: "",
      icon: "warning",
      text: "آیا مایل به حذف  هستید",
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: "بله",
      cancelButtonText: "خیر",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const res = await deleteUser(id, controller.token);
        if (res.success) {
          getData(rowsPerPage, 1, filter, search);
          Swal.fire({
            text: res.message,
            icon: "success",
            confirmButtonText: "بسیار خوب",
          });
        } else {
          Swal.fire({
            text: res.message,
            icon: "error",
            confirmButtonText: "بسیار خوب",
          });
        }
        if (res.errorcode === 401) {
          navigate("/", { replace: true });
        }
      }
    });
  };
  useEffect(() => {
    setDirection(dispatch, "rtl");
     
    getData(10, 1, filter, "");
  }, []);
  const convertUserRole = (type) => {
    let ret = "";
    switch (type) {
      case "saleadmin":
        ret = "مدیر فروش";
        break;
      case "saller":
        ret = "فروشنده";
        break;
        case "designer":
          ret = "طراح";
          break;
          case "production":
            ret = "ساخت تولید";
            break;
      case "productionadmin":
        ret = "مدیر ساخت و تولید";
        break;
      case "accountants":
        ret = "حسابدار";
        break;
      case "web":
        ret = "وبمستر";
        break;
      case "collaborator":
        ret = "همکار";
        break;
      default:
        ret = "";
        break;
    }
    return ret;
  };
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pt={6} pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <Card>
              <MDBox
                mx={2}
                mt={-3}
                py={1}
                px={2}
                variant="gradient"
                bgColor="info"
                borderRadius="lg"
                coloredShadow="info"
              >
                <Grid container>
                  <Grid item xs={12} sm={6}>
                    <MDButton
                      style={{ float: "left" }}
                      variant="contained"
                      color="primary"
                      onClick={() => {
                        navigate("/users/sign-up");
                      }}
                    >
                      ثبت جدید
                    </MDButton>
                  </Grid>
                </Grid>
              </MDBox>
              <MDBox pt={3}>
                <LoadingOverlay className="overlayclass" active={loading} spinner text="لطفا کمی صبر کنید">
                  <Grid container spacing={2} mr={1} ml={1} mb={2}>
                    <Grid item lg={4} md={4} xs={5}>
                      <TextField
                        autoFocus
                        id="search"
                        fullWidth
                        variant="outlined"
                        value={search}
                        placeholder="جستجو..."
                        onChange={(event) => {
                          setSearch(event.target.value);
                          if (event.target.value === "") {
                            setPage(0);
                            getData(rowsPerPage, 1, filter, "");
                          }
                        }}
                        onKeyPress={(ev) => {
                          if (ev.key === "Enter") {
                            setPage(0);
                            getData(rowsPerPage, 1, filter, search);
                          }
                        }}
                      />
                    </Grid>
                    <Grid item lg={4} md={4} xs={5}>
                      <FormControl fullWidth variant="outlined">
                        <InputLabel id="userRole">نوع عضویت</InputLabel>
                        <Select
                          dir="rtl"
                          id="userRole"
                          labelId="label-userRole"
                          label="نوع عضویت"
                          onChange={(event) => {
                            const temp = filter;
                            temp.userRole = event.target.value;
                            setFilter(temp);
                            getData(rowsPerPage, 1, filter, search);
                          }}
                        >
                          <MenuItem value="">همه</MenuItem>
                          <MenuItem value="saleadmin">مدیر فروش</MenuItem>
                          <MenuItem value="saller">فروشنده</MenuItem>
                          <MenuItem value="designer">طراح</MenuItem>
                          <MenuItem value="production">ساخت تولید</MenuItem>
                          <MenuItem value="productionadmin">مدیر ساخت و تولید</MenuItem>
                          <MenuItem value="accountants">حسابدار</MenuItem>
                          <MenuItem value="web">وبمستر</MenuItem>
                          <MenuItem value="collaborator">همکار</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                  </Grid>
                  <TableContainer component={Paper} style={{ borderWidth: 1, borderColor: "#000" }}>
                    <Table  >
                      <TableBody>
                        {/* <TableRow style={{ backgroundColor: "#ccc" }}>
                          <TableCell>
                          </TableCell>
                        </TableRow> */}
                        {datas.map((row, index) => (
                          <TableRow
                            sx={{ "& > *": { borderBottom: "unset" } }}
                            key={row.name}
                            style={{
                              borderWidth: 1,
                              borderColor: "#000",
                              borderRadius: 5,
                            }}
                          >
                            <TableCell>
                              <div className="borderdtbltr">
                                <Grid
                                  className="tblhrader"
                                  container
                                  px={2}
                                  onClick={() => {
                                    if (open === index) {
                                      setOpen();
                                    } else {
                                      setOpen(index);
                                    }
                                  }}
                                >
                                  <Grid lg={4} md={6} xs={12} style={{ textAlign: "right" }}>
                                    <b> نام نام خانوادگی: </b>
                                    {row.name}
                                  </Grid>
                                  <Grid lg={4} md={6} xs={12} style={{ textAlign: "right" }}>
                                    <b> نوع عضویت: </b>
                                    {convertUserRole(row.userRole)}
                                  </Grid>
                                </Grid>
                                {index === open && (
                                  <Collapse in={index === open} timeout="auto" unmountOnExit>
                                    <Grid container px={2} className="tblrow">
                                      <Grid lg={4} md={6} xs={12} style={{ textAlign: "right" }}>
                                        <b> شماره همراه: </b>
                                        {row.phone}
                                      </Grid>
                                      <Grid lg={4} md={6} xs={12} style={{ textAlign: "right" }}>
                                        <b> کدملی: </b>
                                        {row.nationalCode}
                                      </Grid>
                                    </Grid>
                                    <Grid container px={2} style={{ textAlign: "left" }}>
                                      <Tooltip
                                        key={`Tooltipedit${row.id}`}
                                        title="ویرایش"
                                        aria-label="edit"
                                        style={{
                                          float: "left",
                                          cursor: "pointer",
                                          width: 20,
                                          height: 20,
                                        }}
                                      >
                                        <LoopIcon
                                          color="secondary"
                                          onClick={() => {
                                            navigate(`/form/${row.id}`);
                                          }}
                                        />
                                      </Tooltip>

                                      <Tooltip
                                        key={`Tooltipdelete${row.id}`}
                                        title="حذف"
                                        aria-label="delete"
                                        style={{ cursor: "pointer", width: 20, height: 20 }}
                                      >
                                        <DeleteOutlineIcon
                                          color="secondary"
                                          onClick={async () => {
                                            handleDeleteClick(row.id);
                                          }}
                                        />
                                      </Tooltip>
                                    </Grid>
                                  </Collapse>
                                )}
                              </div>
                            </TableCell>
                          </TableRow>
                        ))}
                        {emptyRows > 0 && (
                          <TableRow style={{ height: 0 }}>
                            <TableCell />
                          </TableRow>
                        )}
                      </TableBody>
                      <TableFooter>
                        <TableRow>
                          <TablePagination
                            rowsPerPageOptions={[5, 10, 25, { label: "همه", value: -1 }]}
                            count={count}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                              inputProps: {
                                "aria-label": "سطر در هر صفحه",
                              },
                              native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                          />
                        </TableRow>
                      </TableFooter>
                    </Table>
                  </TableContainer>
                </LoadingOverlay>
              </MDBox>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
    </DashboardLayout>
  );
}

export default Tables;
