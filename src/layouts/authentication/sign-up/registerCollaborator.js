// react-router-dom components
import { useNavigate, useParams } from "react-router-dom";

// @mui material components
import Card from "@mui/material/Card";

// Market Place React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDInput from "components/MDInput";
import MDButton from "components/MDButton";
import Swal from "sweetalert2";
import LoadingOverlay from "react-loading-overlay";

// Authentication layout components
// import Joi from "joi";
import { useEffect, useState } from "react";
import { register, deleteUser, getUser, updateUser } from "controllers/UserController";
import DashboardLayout from "component/LayoutContainers/DashboardLayout";
import DashboardNavbar from "component/Navbars/DashboardNavbar";
import { setDirection, useMaterialUIController } from "context";
import { FormControl, Grid, InputLabel, MenuItem, Select, TextField } from "@mui/material";
import Footer from "component/Footer";

function registerCollaborator() {
  const [name, setName] = useState("");
  const [nationalCode, setNationalCode] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [userRole, setUserRole] = useState("collaborator");
  const [gallery, setGallery] = useState("");
  const [collaboratorType, setCollaboratorType] = useState("");
  const [loading, setLoading] = useState(false);
  const [controller, dispatch] = useMaterialUIController();
  const { id } = useParams();
  const navigate = useNavigate();
  const getData = async () => {
    setLoading(true);
    const res = await getUser({ id }, controller.token);
    setLoading(false);
    if (res.success) {
      setName(res.data.name);
      setPhone(res.data.phone);
      setNationalCode(res.data.nationalCode);
      setAddress(res.data.address);
      setUserRole(res.data.userRole);
      setGallery(res.data.gallery);
      setCollaboratorType(res.data.collaboratorType);
    } else {
      Swal.fire({
        text: `${res.message} (${res.code})`,
        icon: "error",
        confirmButtonText: "OK",
      });
    }
    if (res.code === 401) {
      navigate("/", { replace: true });
    }
  };
  const handleSubmitClick = async () => {
    // const schema = Joi.object({
    //   name: Joi.string()
    //     .required()
    //     .min(5)
    //     .max(256)
    //     .messages({ "string.empty": "نام و نام خانوادگی را وارد نمایید" }),
    //   phone: Joi.string()
    //     .min(10)
    //     .max(12)
    //     .messages({ "string.empty": "شماره موبایل را وارد نمایید" }),
    //   nationalCode: Joi.string()
    //     .min(10)
    //     .max(10)
    //     .messages({ "string.empty": "کدملی را وارد نمایید" }),
    //   userRole: Joi.string().required().messages({ "string.empty": "نوع فعالیت را انتخاب نمایید" }),
    // });

    // const { error } = schema.validate({
    //   name,
    //   phone,
    //   userRole,
    // });

    // if (error) {
    //   Swal.fire({
    //     text: `${error.details[0].message}  `,
    //     icon: "error",
    //     confirmButtonText: "OK",
    //   });
    //   return;
    // }
    setLoading(true);
    const newUser = {
      name,
      phone: phone.replace(/[۰-۹]/g, (d) => "۰۱۲۳۴۵۶۷۸۹".indexOf(d)),
      nationalCode: nationalCode.replace(/[۰-۹]/g, (d) => "۰۱۲۳۴۵۶۷۸۹".indexOf(d)),
      address,
      userRole,
      gallery,
      collaboratorType,
    };
    let res;
    if (id) {
      newUser.id = id;
      res = await updateUser(newUser);
    } else {
      res = await register(newUser, controller.token);
    }
    setLoading(false);
    if (res.success) {
      Swal.fire({
        html: res.message,
        icon: "success",
        confirmButtonText: "Ok",
      });
      navigate("/users/collaborator", { replace: true });
      // clearForm();
    } else {
      Swal.fire({
        text: `${res.message} (${res.code})`,
        icon: "error",
        confirmButtonText: "OK",
      });
    }
    if (res.code === 401) {
      navigate("/", { replace: true });
    }
  };
  const handleDeleteClick = async () => {
    Swal.fire({
      title: "",
      icon: "warning",
      text: "آیا مایل به حذف  هستید",
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: "بله",
      cancelButtonText: "خیر",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const res = await deleteUser(id, controller.token);
        if (res.success) {
          Swal.fire({
            text: res.message,
            icon: "success",
            confirmButtonText: "بسیار خوب",
          });
          if (controller.authenticated) {
            navigate("/users/collaborator", { replace: true });
          } else {
            navigate("/");
          }
        } else {
          Swal.fire({
            text: res.message,
            icon: "error",
            confirmButtonText: "بسیار خوب",
          });
        }
        if (res.errorcode === 401) {
          navigate("/", { replace: true });
        }
      }
    });
  };
  useEffect(() => {
    setDirection(dispatch, "rtl");
    if (id) {
      getData();
    }
  }, []);

  return (
    <DashboardLayout>
      <DashboardNavbar title="همکار" />
      <MDBox pt={6} pb={3}>
        <Card>
          <MDBox
            variant="gradient"
            bgColor="info"
            borderRadius="lg"
            coloredShadow="success"
            mx={2}
            mt={-3}
            p={1}
            mb={1}
            textAlign="center"
          >
            <MDTypography variant="h6" fontWeight="medium" color="white">
              همکار
            </MDTypography>
          </MDBox>
          <MDBox pt={4} pb={3} px={3}>
            <MDBox component="form" role="form">
              <LoadingOverlay
                className="overlayclass"
                active={loading}
                spinner
                text=" لطفا کمی صبر کنید"
              >
                <MDBox mb={2}>
                  <TextField
                    type="text"
                    label="نام و نام خانوادگی"
                    variant="outlined"
                    fullWidth
                    value={`${name}`}
                    onChange={(event) => {
                      setName(event.target.value);
                    }}
                  />
                </MDBox>
                <MDBox mb={2}>
                  <MDInput
                    type="tel"
                    label="شماره موبایل(نام کاربری)"
                    variant="outlined"
                    fullWidth
                    disabled={id}
                    value={`${phone}`}
                    onChange={(event) => {
                      setPhone(event.target.value);
                    }}
                  />
                </MDBox>
                <MDBox mb={2}>
                  <MDInput
                    type="tel"
                    label="کدملی"
                    variant="outlined"
                    fullWidth
                    value={`${nationalCode}`}
                    onChange={(event) => {
                      setNationalCode(event.target.value);
                    }}
                  />
                </MDBox>
                <MDBox mb={2}>
                  <MDInput
                    label="آدرس"
                    variant="outlined"
                    fullWidth
                    value={address}
                    onChange={(event) => {
                      setAddress(event.target.value);
                    }}
                  />
                </MDBox>
                <MDBox mb={2}>
                  <FormControl fullWidth variant="outlined">
                    <InputLabel id="userRole">نوع عضویت</InputLabel>
                    <Select
                      dir="rtl"
                      id="userRole"
                      labelId="label-userRole"
                      label="نوع عضویت"
                      value={"collaborator"}
                      onChange={(event) => {
                        setUserRole(event.target.value);
                      }}
                    >
                      <MenuItem value="collaborator">همکار</MenuItem>
                    </Select>
                  </FormControl>
                </MDBox>
                {userRole === "collaborator" && (
                  <div>
                    <MDBox mb={2}>
                      <MDInput
                        label="نام گالری"
                        variant="outlined"
                        fullWidth
                        value={gallery}
                        onChange={(event) => {
                          setGallery(event.target.value);
                        }}
                      />
                    </MDBox>
                    <MDBox mb={2}>
                      <FormControl fullWidth variant="outlined">
                        <InputLabel id="collaboratorType">نوع فعالیت</InputLabel>
                        <Select
                          id="collaboratorType"
                          labelId="label-collaboratorType"
                          label="نوع فعالیت"
                          value={collaboratorType}
                          onChange={(event) => {
                            setCollaboratorType(event.target.value);
                          }}
                        >
                          <MenuItem value={null}>
                            <em>None</em>
                          </MenuItem>
                          <MenuItem value="workshop">کارگاه</MenuItem>
                          <MenuItem value="goldsmith">زرگر</MenuItem>
                          <MenuItem value="saleadmin">نقره فروش</MenuItem>
                        </Select>
                      </FormControl>
                    </MDBox>
                  </div>
                )}
                <Grid container spaceing={2} mt={4} mb={1} style={{ justifyContent: "flex-end" }}>
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={() => {
                      navigate("/users/collaborator");
                    }}
                  >
                    انصراف
                  </MDButton>
                  {id && (
                    <MDButton
                      style={{ marginRight: 8, marginLeft: 8 }}
                      variant="gradient"
                      color="error"
                      onClick={() => {
                        handleDeleteClick();
                      }}
                    >
                      Delete
                    </MDButton>
                  )}
                  <MDButton
                    style={{ marginRight: 8, marginLeft: 8 }}
                    variant="gradient"
                    color="info"
                    onClick={() => {
                      handleSubmitClick();
                    }}
                  >
                    {id ? "ویرایش" : "ثبت"}
                  </MDButton>
                </Grid>
              </LoadingOverlay>
            </MDBox>
          </MDBox>
        </Card>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}

export default registerCollaborator;
