/**
=========================================================
* mohtasham React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

import { useEffect, useState } from "react";

// react-router-dom components

// @mui material components
import Card from "@mui/material/Card";

// mohtasham React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import LoadingOverlay from "react-loading-overlay";
import validator from "validator";
import Swal from "sweetalert2";
// Authentication layout components
import BasicLayout from "layouts/authentication/components/BasicLayout";

// Images
import bgImage from "assets/images/bg-sign-in-basic.jpeg";
import { setLogin, setDirection, useMaterialUIController } from "context";
import { login, startLogin } from "controllers/UserController";
import { TextField } from "@mui/material";

let validationMessage = "";
function Basic() {
  const [username, setUsername] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [sendSms, setSendSms] = useState(false);
  const [, dispatch] = useMaterialUIController();

  const validation = [];
  let isValid = true;
  let message = "";
  const validate = () => {
    validationMessage = "";
    if (validator.isEmpty(username)) {
      isValid = false;
      message += "لطفا شماره موبایل را وارد نمایید<br />";
    }
    if (validator.isEmpty(password)) {
      isValid = false;
      message += "لطفا رمزعبور را وارد نمایید<br />";
    }
    validationMessage = message;
    return isValid;
  };

  const countDown = () => {
    setTimeout(() => {
      setSendSms(true);
    }, 60000);
  };
  const loginHandler = async (pass) => {
    if (!validate()) {
      Swal.fire({
        html: validationMessage,
        icon: "error",
        confirmButtonText: "بسیار خوب",
      });
      return;
    }

    if (validation.length === 0) {
      setLoading(true);
      let passs = password;
      if (pass) {
        passs = pass;
      }
      const res = await login(
        username.replace(/[۰-۹]/g, (d) => "۰۱۲۳۴۵۶۷۸۹".indexOf(d)),
        passs.replace(/[۰-۹]/g, (d) => "۰۱۲۳۴۵۶۷۸۹".indexOf(d))
      );

      if (res.success) {
        setLogin(dispatch, {
          token: res.data.token,
          userRole: res.data.userRole,
          id: res.data.id,
          authenticated: true,
          name: res.data.name,
        });
        if (res.data.userRole == "web") {
          window.location.href = "/catalogues";
        } 
        // else if (res.data.userRole == "collaborator") {
        //   window.location.href = "/catalogueview";
        // } 
        else {
          window.location.href = "/dashboard";
        }
      } else {
        Swal.fire({
          text: res.message,
          icon: "error",
          confirmButtonText: "بسیار خوب",
        });
      }
      setLoading(false);
    }
  };
  const loginHandler1 = async () => {
    if (validator.isEmpty(username)) {
      isValid = false;
      Swal.fire({
        text: "لطفا نام کاربری را وارد نمایید",
        icon: "error",
        confirmButtonText: "بسیار خوب",
      });

      return;
    }

    setLoading(true);
    const res = await startLogin(username.replace(/[۰-۹]/g, (d) => "۰۱۲۳۴۵۶۷۸۹".indexOf(d)));
    if (res.success) {
      countDown();
      setShowPassword(true);
      setPassword("");
      setSendSms(false);
    } else {
      Swal.fire({
        text: res.message,
        icon: "error",
        confirmButtonText: "بسیار خوب",
      });
    }
    setLoading(false);
  };
  useEffect(() => {
    setDirection(dispatch, "rtl");

    dispatch({
      type: "logout",
    });
  }, []);
  return (
    <BasicLayout image={bgImage}>
      <Card>
        <MDBox
          variant="gradient"
          bgColor="info"
          borderRadius="lg"
          coloredShadow="info"
          mx={2}
          mt={-3}
          p={2}
          mb={1}
          textAlign="center"
        >
          <MDTypography variant="h4" fontWeight="medium" color="white" mt={1}>
            ورود به حساب کاربری
          </MDTypography>
        </MDBox>
        <MDBox pt={4} pb={3} px={3}>
          <LoadingOverlay
            className="overlayclass"
            active={loading}
            spinner
            text=" لطفا کمی صبر کنید"
          >
            <MDBox component="form" role="form">
              <MDBox mb={2}>
                <TextField
                  id="cell"
                  type="tel"
                  label="شماره موبایل"
                  placeholder="09000000000"
                  fullWidth
                  value={username}
                  variant="outlined"
                  onChange={(event) => {
                    setUsername(event.target.value);
                  }}
                />
              </MDBox>
              {showPassword && (
                <MDBox mb={2}>
                  <TextField
                    id="password"
                    type="password"
                    label="رمزعبور"
                    fullWidth
                    value={password}
                    variant="outlined"
                    onChange={(event) => {
                      setPassword(event.target.value);
                      // if (event.target.value.length === 6) {
                      //   loginHandler(event.target.value);
                      // }
                    }}
                  />
                </MDBox>
              )}
              <MDBox mb={2}>
                <MDButton
                  variant="gradient"
                  color="info"
                  onClick={() => {
                    if (showPassword) {
                      loginHandler(password);
                    } else {
                      loginHandler1();
                    }
                  }}
                  style={{ float: "left", minWidth: 120 }}
                >
                  ورود
                </MDButton>
                {sendSms && (
                  <MDButton
                    color="secondary"
                    style={{ float: "left", minWidth: 120, marginRight: 5 }}
                    onClick={() => {
                      loginHandler1();
                    }}
                  >
                    ارسال مجدد رمز
                  </MDButton>
                )}
              </MDBox>
              {/* <Grid container>
                <MDBox mt={3} mb={1} textAlign="center">
                  <MDTypography variant="button" color="text">
                    ثبت نام نکرده اید؟
                    <MDTypography
                      component={Link}
                      to="/authentication/sign-up"
                      variant="button"
                      color="info"
                      fontWeight="medium"
                      textGradient
                    >
                      ایجاد حساب کاربری
                    </MDTypography>
                  </MDTypography>
                </MDBox>
              </Grid> */}
            </MDBox>
          </LoadingOverlay>
        </MDBox>
      </Card>
    </BasicLayout>
  );
}

export default Basic;
