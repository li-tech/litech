import { useEffect, useState } from "react";
// @mui material components
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
// Acromegaly React components
import MDBox from "components/MDBox";
import Swal from "sweetalert2";
import LoadingOverlay from "react-loading-overlay";
// Acromegaly React example components
import DashboardLayout from "component/LayoutContainers/DashboardLayout";
import DashboardNavbar from "component/Navbars/DashboardNavbar";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUp from "@mui/icons-material/KeyboardArrowUp";
import PropTypes from "prop-types";
// Acromegaly React contexts
import { useMaterialUIController, setDirection } from "context";
import { useNavigate, useParams } from "react-router-dom";
import { Box, Button, FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material";
import { getUsersList } from "controllers/UserController";
import MDButton from "components/MDButton";
import MDTypography from "components/MDTypography";
import Footer from "component/Footer";
import { verifyUser } from "controllers/UserController";

function TablePaginationActions(props) {
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5, direction: "rtl" }}>
      <IconButton onClick={handleFirstPageButtonClick} disabled={page === 0} aria-label="صفحه اول">
        <LastPageIcon />
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        <KeyboardArrowRight />
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="اخرین صفحه"
      >
        <KeyboardArrowLeft />
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="صفحه قبلی"
      >
        <FirstPageIcon />
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const filters = {
  userRole: "",
};

function Tables() {
  const [controller, dispatch] = useMaterialUIController();
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(0);
  const [filter, setFilter] = useState(filters);
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(0);
  const [open, setOpen] = useState();
  const [datas, setDatas] = useState([]);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const { role } = useParams();
  const navigate = useNavigate();
  let emptyRows = rowsPerPage - Math.min(rowsPerPage, count - page * rowsPerPage);

  // Avoid a layout jump when reaching the last page with empty rows.
  const getData = async (perPagex, pagex, filterx, searchx) => {
    setLoading(true);
    setDatas([]);

    const data = await getUsersList(perPagex, pagex, searchx, filterx, controller.token);
    if (data.success) {
      setDatas(data.data);

      setCount(data.count);
      emptyRows = rowsPerPage - Math.min(rowsPerPage, datas.length - (pagex - 1) * rowsPerPage);
    } else {
      Swal.fire({
        text: data.message,
        icon: "error",
        confirmButtonText: "بسیار خوب",
      });

      if (data.code === 401) {
        navigate("/", { replace: true });
      }
    }

    setLoading(false);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    getData(rowsPerPage, newPage + 1, filter, search);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setFilter(filters);
    getData(parseInt(event.target.value, 10), 1, filter, search);
  };
  const handleVerifyClick = async (id, verified) => {
    Swal.fire({
      title: "",
      icon: "warning",
      text: "آیا مطنين هستید",
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: "بله",
      cancelButtonText: "خیر",
    }).then(async (result) => {
      if (result.isConfirmed) {
        let newData = {
          id,
          verified,
        };
        const res = await verifyUser(newData, controller.token);
        if (res.success) {
          Swal.fire({
            text: res.message,
            icon: "success",
            confirmButtonText: "بسیار خوب",
          });
          if (controller.authenticated) {
            getData(rowsPerPage, 1, filter, search);
          } else {
            navigate("/");
          }
        } else {
          Swal.fire({
            text: res.message,
            icon: "error",
            confirmButtonText: "بسیار خوب",
          });
        }
        if (res.errorcode === 401) {
          navigate("/", { replace: true });
        }
      }
    });
  };
  useEffect(() => {
    setDirection(dispatch, "rtl");
  
    const temp = filter;
    if (role) {
      temp.userRole = role;
    } else {
      temp.userRole = "";
    }
    setFilter(temp);
    getData(10, 1, temp, "");
    return () => {
      setFilter();
    };
  }, []);
  const convertUserRole = (type) => {
    let ret = "";
    switch (type) {
      case "saleadmin":
        ret = "مدیر فروش";
        break;
      case "saller":
        ret = "فروشنده";
        break;
        case "designer":
          ret = "طراح";
          break;
          case "production":
            ret = "ساخت تولید";
            break;
      case "productionadmin":
        ret = "مدیر ساخت و تولید";
        break;
      case "accountants":
        ret = "حسابدار";
        break;
      case "web":
        ret = "وبمستر";
        break;
      case "collaborator":
        ret = "همکار";
        break;
      case "supperadmin":
        ret = "مدیرسیستم";
        break;
      default:
        ret = "";
        break;
    }
    return ret;
  };
  return (
    <DashboardLayout>
      <DashboardNavbar title={role ? "لیست همکاران" : "لیست کاربران"} />
      <MDBox pt={6} pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <Card>
              <MDBox
                mx={2}
                mt={-3}
                py={1}
                px={2}
                variant="gradient"
                bgColor="info"
                borderRadius="lg"
                coloredShadow="info"
              >
                <Grid container>
                  <Grid item xs={6} sm={6}>
                    <MDTypography variant="h6" fontWeight="medium" color="white">
                      {role ? "لیست همکاران" : "لیست کاربران"}
                    </MDTypography>
                  </Grid>
                  <Grid item xs={6} sm={6} style={{ justifyContent: "flex-end" }}>
                    <MDButton
                      style={{ float: "left" }}
                      variant="contained"
                      color="primary"
                      onClick={() => {
                        if (role) {
                          navigate("/authentication/registerCollaborator");
                        } else {
                          navigate("/authentication/sign-up/");
                        }
                      }}
                    >
                      ثبت جدید
                    </MDButton>
                  </Grid>
                </Grid>
              </MDBox>
              <MDBox pt={3}>
                <LoadingOverlay
                  className="overlayclass"
                  active={loading}
                  spinner
                  text="لطفا کمی صبر کنید"
                >
                  <Grid container spacing={2} mr={1} ml={1} mb={2}>
                    <Grid item lg={4} md={4} xs={5}>
                      <TextField
                        autoFocus
                        id="search"
                        fullWidth
                        variant="outlined"
                        value={search}
                        placeholder="جستجو..."
                        onChange={(event) => {
                          setSearch(event.target.value);
                          if (event.target.value === "") {
                            setPage(0);
                            getData(rowsPerPage, 1, filter, "");
                          }
                        }}
                        onKeyPress={(ev) => {
                          if (ev.key === "Enter") {
                            setPage(0);
                            getData(rowsPerPage, 1, filter, search);
                          }
                        }}
                      />
                    </Grid>
                    {!role && (
                      <Grid item lg={4} md={4} xs={5}>
                        <FormControl fullWidth variant="outlined">
                          <InputLabel id="userRole">نوع عضویت</InputLabel>
                          <Select
                            dir="rtl"
                            id="userRole"
                            labelId="label-userRole"
                            label="نوع عضویت"
                            value={filter.userRole}
                            onChange={(event) => {
                              const temp = filter;
                              temp.userRole = event.target.value;
                              setFilter(temp);
                              getData(rowsPerPage, 1, filter, search);
                            }}
                          >
                            <MenuItem value="">همه</MenuItem>
                            <MenuItem value="saleadmin">مدیر فروش</MenuItem>
                            <MenuItem value="saller">فروشنده</MenuItem>
                            <MenuItem value="designer">طراح</MenuItem>
                            <MenuItem value="production">ساخت تولید</MenuItem>
                            <MenuItem value="productionadmin">مدیر ساخت و تولید</MenuItem>
                            <MenuItem value="accountants">حسابدار</MenuItem>
                            <MenuItem value="web">وبمستر</MenuItem>
                            <MenuItem value="collaborator">همکار</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                    )}
                  </Grid>
                  <TableContainer component={Paper} style={{ borderWidth: 1, borderColor: "#000" }}>
                    <Table>
                      <TableBody>
                        {/* <TableRow style={{ backgroundColor: "#ccc" }}>
                          <TableCell>
                          </TableCell>
                        </TableRow> */}
                        {datas.map((row, index) => (
                          <TableRow
                            sx={{ "& > *": { borderBottom: "unset" } }}
                            key={row.name}
                            style={{
                              borderWidth: 1,
                              borderColor: "#000",
                              borderRadius: 5,
                            }}
                          >
                            <TableCell>
                              <div className="borderdtbltr">
                                <Grid className="tblhrader" container px={1}>
                                  <Grid
                                    container
                                    lg={10}
                                    md={10}
                                    xs={9}
                                    onClick={() => {
                                      if (role) {
                                        navigate(`/authentication/registerCollaborator/${row._id}`);
                                      } else {
                                        navigate(`/authentication/sign-up/${row._id}`);
                                      }
                                    }}
                                  >
                                    <Grid item lg={4} md={6} xs={12} style={{ textAlign: "right" }}>
                                      <b> نام نام خانوادگی: </b>
                                      {row.name}
                                    </Grid>
                                    <Grid item lg={4} md={6} xs={12} style={{ textAlign: "right" }}>
                                      <b> نوع عضویت: </b>
                                      {convertUserRole(row.userRole)}
                                    </Grid>
                                    {index === open && (
                                      <>
                                        <Grid
                                          item
                                          lg={4}
                                          md={6}
                                          xs={12}
                                          style={{ textAlign: "right" }}
                                        >
                                          <b> شماره همراه: </b>
                                          {row.phone}
                                        </Grid>
                                        <Grid
                                          item
                                          lg={4}
                                          md={6}
                                          xs={12}
                                          style={{ textAlign: "right" }}
                                        >
                                          <b> کدملی: </b>
                                          {row.nationalCode}
                                        </Grid>
                                        {row.userRole === "collaborator" && (
                                          <>
                                            <Grid
                                              item
                                              lg={4}
                                              md={6}
                                              xs={12}
                                              style={{ textAlign: "right" }}
                                            >
                                              <b> نام گالری: </b>
                                              {row.gallery}
                                            </Grid>
                                            <Grid
                                              item
                                              lg={4}
                                              md={6}
                                              xs={12}
                                              style={{ textAlign: "right" }}
                                            >
                                              <b> نوع فعالیت: </b>
                                              {row.collaboratorType}
                                            </Grid>
                                          </>
                                        )}
                                        <Grid
                                          item
                                          lg={4}
                                          md={6}
                                          xs={12}
                                          style={{ textAlign: "right" }}
                                        >
                                          <b> آدرس: </b>
                                          {row.address}
                                        </Grid>
                                      </>
                                    )}
                                  </Grid>
                                  <Grid
                                    lg={2}
                                    md={2}
                                    xs={3}
                                    style={{
                                      alignItems: "flex-start",
                                      justifyContent: "flex-start",
                                    }}
                                  >
                                    <div
                                      onClick={() => {
                                        if (open === index) {
                                          setOpen();
                                        } else {
                                          setOpen(index);
                                        }
                                      }}
                                    >
                                      {index !== open && (
                                        <KeyboardArrowDownIcon
                                          fontSize="small"
                                          style={{
                                            lineHeight: "100%",
                                            fontSize: 20,
                                            width: 35,
                                            height: 35,
                                          }}
                                        />
                                      )}
                                      {index === open && (
                                        <KeyboardArrowUp
                                          style={{
                                            lineHeight: "100%",
                                            fontSize: 20,
                                            width: 35,
                                            height: 35,
                                          }}
                                        />
                                      )}
                                    </div>
                                    <Button
                                      style={{
                                        padding: 1,
                                        margin: 1,
                                      }}
                                      onClick={() => {
                                        let veri = row.verified ? false : true;
                                        handleVerifyClick(row._id, veri);
                                      }}
                                    >
                                      {row.verified ? "غیرفعال کن" : "فعال کن"}
                                    </Button>
                                  </Grid>
                                </Grid>
                              </div>
                            </TableCell>
                          </TableRow>
                        ))}
                        {emptyRows > 0 && (
                          <TableRow style={{ height: 0 }}>
                            <TableCell />
                          </TableRow>
                        )}
                      </TableBody>
                      <TableFooter>
                        <TableRow>
                          <TablePagination
                            rowsPerPageOptions={[5, 10, 25, { label: "همه", value: -1 }]}
                            count={count}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                              inputProps: {
                                "aria-label": "سطر در هر صفحه",
                              },
                              native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                          />
                        </TableRow>
                      </TableFooter>
                    </Table>
                  </TableContainer>
                </LoadingOverlay>
              </MDBox>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Tables;
