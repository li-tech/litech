// @mui material components
import Grid from "@mui/material/Grid";

// mohtasham React components
import MDBox from "components/MDBox";

// mohtasham React example components
import DashboardLayout from "component/LayoutContainers/DashboardLayout";
import DashboardNavbar from "component/Navbars/DashboardNavbar";
import ReportsBarChart from "component/Charts/BarCharts/ReportsBarChart";
import ReportsLineChart from "component/Charts/LineCharts/ReportsLineChart";
import ComplexStatisticsCard from "component/Cards/StatisticsCards/ComplexStatisticsCard";
import ProductionQuantityLimitsOutlinedIcon from "@mui/icons-material/ProductionQuantityLimitsOutlined";
import ShoppingCartCheckoutOutlinedIcon from "@mui/icons-material/ShoppingCartCheckoutOutlined";
import GroupOutlinedIcon from "@mui/icons-material/GroupOutlined";
import PeopleOutlineIcon from "@mui/icons-material/PeopleOutline";
import PointOfSaleOutlinedIcon from "@mui/icons-material/PointOfSaleOutlined";
import CollectionsOutlinedIcon from '@mui/icons-material/CollectionsOutlined';
// Data
import reportsBarChartData from "layouts/dashboard/data/reportsBarChartData";
import reportsLineChartData from "layouts/dashboard/data/reportsLineChartData";
import ContactsIcon from '@mui/icons-material/Contacts';
import InfoIcon from '@mui/icons-material/Info';

// mohtasham React contexts
import { useMaterialUIController, setDirection } from "context";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { getDashboard } from "controllers/BasicInformationController";
import Footer from "component/Footer";
import { setLogin } from "context";

function Dashboard() {
  const [controller, dispatch] = useMaterialUIController();
  const { sales, tasks } = reportsLineChartData;
  const [data, setData] = useState();

  const navigate = useNavigate();
  // Changing the direction to rtl
  const getDatas = async () => {
    const res = await getDashboard(controller.token);
    
    if (res.success) {
      setData(res);
      setLogin(dispatch, {
        token: res.newUser.token,
        userRole: res.newUser.userRole,
        id: res.newUser.id,
        authenticated: true,
        name: res.newUser.name,
      });
    }
  };
  useEffect(() => {
     
    setDirection(dispatch, "rtl");
    getDatas();
  }, []);
  return (
    <DashboardLayout>
      {controller.authenticated && <DashboardNavbar title="داشبورد" />}
      <MDBox py={3}>
        <Grid container spacing={3} style={{}}>
          {["supperadmin", "saleadmin", "saller", "productionadmin"].filter(
            (a) => a == controller.userRole
          ).length > 0 && (
            <Grid item xs={12} md={6} lg={3}>
              <MDBox mb={1.5}>
                <ComplexStatisticsCard
                  color="dark"
                  icon={<ProductionQuantityLimitsOutlinedIcon />}
                  title="ثبت سفارش "
                  count="-"
                  btns={[
                    {
                      text: "ثبت سفارش همکار",
                      click: () => {
                        window.location.href = "neworder/0/new/0";
                      },
                    },
                    {
                      text: "ثبت سفارش مشتری",
                      click: () => {
                        window.location.href = "/neworder/1/new/0";
                      },
                    },
                  ]}
                />
              </MDBox>
            </Grid>
          )}

          {["supperadmin", "saleadmin", "saller", "productionadmin", "designer","production"].filter(
            (a) => a == controller.userRole
          ).length > 0 && (
            <Grid item xs={12} md={6} lg={3}>
              <MDBox mb={1.5}>
                <ComplexStatisticsCard
                  color="dark"
                  icon={<ProductionQuantityLimitsOutlinedIcon />}
                  title="سفارش های در جال اجرا"
                  count={data && data.activeOrders}
                  btns={[
                    {
                      text: "لیست سفارشات درحال اجرا",
                      click: () => {
                        navigate("/activeorders");
                      },
                    },
                  ]}
                />
              </MDBox>
            </Grid>
          )}
          {["supperadmin", "saleadmin", "saller", "productionadmin", "designer","production"].filter(
            (a) => a == controller.userRole
          ).length > 0 && (
            <Grid item xs={12} md={6} lg={3}>
              <MDBox mb={1.5}>
                <ComplexStatisticsCard
                  icon={<ShoppingCartCheckoutOutlinedIcon />}
                  title="سفارشات ارسال شده"
                  count={data && data.deActiveOrders}
                  btns={[
                    {
                      text: "لیست سفارشات ارسال شده",
                      click: () => {
                        navigate("/deactiveorders");
                      },
                    },
                  ]}
                />
              </MDBox>
            </Grid>
          )}
          {["supperadmin", "saleadmin", "productionadmin", "accountants"].filter(
            (a) => a == controller.userRole
          ).length > 0 && (
            <Grid item xs={12} md={6} lg={3}>
              <MDBox mb={1.5}>
                <ComplexStatisticsCard
                  color="primary"
                  icon={<PointOfSaleOutlinedIcon />}
                  title={"حسابداری همکاران"}
                  count="-"
                  btns={[
                    {
                      text: "حسابداری همکاران",
                      click: () => {
                        navigate("/collaborator");
                      },
                    },
                  ]}
                />
              </MDBox>
            </Grid>
          )}
          {["supperadmin", "saleadmin", "saller", "accountants"].filter(
            (a) => a == controller.userRole
          ).length > 0 && (
            <Grid item xs={12} md={6} lg={3}>
              <MDBox mb={1.5}>
                <ComplexStatisticsCard
                  icon={<PointOfSaleOutlinedIcon />}
                  title={"حسابداری  مشتری"}
                  count="-"
                  btns={[
                    {
                      text: "حسابداری مشتری",
                      click: () => {
                        navigate("/collaboratordetails/635682d06ceb014b0abbad6d/2/مشتریان");
                      },
                    },
                  ]}
                />
              </MDBox>
            </Grid>
          )}
          {["supperadmin", "productionadmin"].filter((a) => a == controller.userRole).length >
            0 && (
            <Grid item xs={12} md={6} lg={3}>
              <MDBox mb={1.5}>
                <ComplexStatisticsCard
                  color="success"
                  icon={<GroupOutlinedIcon />}
                  title="همکاران"
                  count={data && data.collaborators}
                  btns={[
                    {
                      text: "لیست همکاران",
                      click: () => {
                        navigate("/users/collaborator");
                      },
                    },
                  ]}
                />
              </MDBox>
            </Grid>
          )}
          {["supperadmin"].filter((a) => a == controller.userRole).length > 0 && (
            <Grid item xs={12} md={6} lg={3}>
              <MDBox mb={1.5}>
                <ComplexStatisticsCard
                  color="primary"
                  icon={<PeopleOutlineIcon />}
                  title="کاربران"
                  count={data && data.users}
                  btns={[
                    {
                      text: "لیست کاربران",
                      click: () => {
                        navigate("/users");
                      },
                    },
                  ]}
                />
              </MDBox>
            </Grid>
          )}
             <Grid item xs={12} md={6} lg={3}>
              <MDBox mb={1.5}>
                <ComplexStatisticsCard
                    icon={<CollectionsOutlinedIcon />}
                  title="کاتالوگ  "
                  count={"-"}
                  btns={[
                    {
                      text: "مشاهده کاتالوگ",
                      click: () => {
                        navigate("/catalogueview");
                      },
                    },
                  ]}
                />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <MDBox mb={1.5}>
                <ComplexStatisticsCard
                   icon={<ContactsIcon />}
                  title="ارتباط با ما  "
                  count={"-"}
                  btns={[
                    {
                      text: "ارتباط با ما ",
                      click: () => {
                        navigate("/contactus");
                      },
                    },
                  ]}
                />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <MDBox mb={1.5}>
                <ComplexStatisticsCard
                  
                  icon={<InfoIcon />}
                  title=" درباره ما  "
                  count={"-"}
                  btns={[
                    {
                      text: "درباره ما ",
                      click: () => {
                        navigate("/aboutus");
                      },
                    },
                  ]}
                />
              </MDBox>
            </Grid>
        </Grid>
        {/* <MDBox mt={4.5}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={6} lg={4}>
              <MDBox mb={3}>
                <ReportsBarChart
                  color="info"
                  title="website views"
                  description="Last Campaign Performance"
                  chart={reportsBarChartData}
                />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} lg={4}>
              <MDBox mb={3}>
                <ReportsLineChart
                  color="success"
                  title="daily sales"
                  description={
                    <>
                      (<strong>+15%</strong>) increase in today sales.
                    </>
                  }
                  chart={sales}
                />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} lg={4}>
              <MDBox mb={3}>
                <ReportsLineChart
                  color="dark"
                  title="completed tasks"
                  description="Last Campaign Performance"
                  chart={tasks}
                />
              </MDBox>
            </Grid>
          </Grid>
        </MDBox> */}
      </MDBox>
      {controller.authenticated && <Footer />}
    </DashboardLayout>
  );
}

export default Dashboard;
