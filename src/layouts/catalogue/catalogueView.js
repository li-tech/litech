// react-router-dom components
import { useNavigate } from "react-router-dom";

// @mui material components
import Card from "@mui/material/Card";

// Market Place React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import Swal from "sweetalert2";
import LoadingOverlay from "react-loading-overlay";

// Authentication layout components
// import Joi from "joi";
import { useEffect, useState } from "react";

import DashboardLayout from "component/LayoutContainers/DashboardLayout";
import DashboardNavbar from "component/Navbars/DashboardNavbar";
import { setDirection, useMaterialUIController } from "context";
import Footer from "component/Footer"; 
import config from "config";
import {
  Box,
  Chip,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
} from "@mui/material";
import { ImageViewer } from "react-image-viewer-dv";
import { ImageSearch, ManageSearch } from "@mui/icons-material";

const allowedRoles = ["supperadmin", "web"];
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
function catalogueView() {
  const [images, setImages] = useState();
  const [text, setText] = useState("");
  const [loading, setLoading] = useState(false);
  const [categories, setCategories] = useState([]);
  const [category, setCategory] = useState();
  const [categoriesList, setCategoriesList] = useState();

  const [controller, dispatch] = useMaterialUIController();
  const navigate = useNavigate();

  const getData = async (searchText, filterParam) => {
    setLoading(true);
    let newDatas = { perPage: 10000, page: 1, search: "" };
    let resList =  {
      success:true,
      data:[]
    };
    if (resList.success) {
      setCategoriesList(resList.data);
    } else {
      Swal.fire({
        text: `${resList.message} (${resList.code})`,
        icon: "error",
        confirmButtonText: "OK",
      });
    }
    if (resList.code === 401) {
      navigate("/", { replace: true });
    }

    newDatas = { perPage: 10000, page: 1, search: searchText, filter: { categories: filterParam } };
    resList =  {
      success:true,
      data:[]
    };
    setLoading(false);
    if (resList.success) {
      const image = [];
      resList.data.forEach((item) => {
        image.push({ url: `${config.fileserver}catalog/${item.picture}`, text: item.text });
      });
      setImages(image);
    } else {
      Swal.fire({
        text: `${resList.message} (${resList.code})`,
        icon: "error",
        confirmButtonText: "OK",
      });
    }
    if (resList.code === 401) {
      navigate("/", { replace: true });
    }
  };
  const handleCategoryChange = (event) => {
    const {
      target: { value },
    } = event;
    const temp = typeof value === "string" ? value.split(",") : value;

    setCategory(temp);
    getData(text, temp);
    setCategories(temp);
  };

  useEffect(() => {

    setDirection(dispatch, "rtl");

    getData();
  }, []);

  return (
    <DashboardLayout>
      <DashboardNavbar title="کاتالوگ" />
      <MDBox pt={6} pb={3}>
        <Card>
          <MDBox
            variant="gradient"
            bgColor="info"
            borderRadius="lg"
            coloredShadow="success"
            mx={2}
            mt={-3}
            p={1}
            mb={1}
            textAlign="center"
          >
            <MDTypography variant="h6" fontWeight="medium" color="white">
              کاتالوگ
            </MDTypography>
          </MDBox>
          <MDBox pt={4} pb={3} px={3}>
            <MDBox component="form" role="form">
              <LoadingOverlay
                className="overlayclass"
                active={loading}
                spinner
                text=" لطفا کمی صبر کنید"
              >
                <MDBox mb={2}>
                  <MDBox mb={2}>
                    <Grid container spacing={2} mb={1}>
                      {categoriesList && (
                        <Grid item lg={6} md={6} xs={12}>
                          <FormControl fullWidth>
                            <InputLabel id="demo-multiple-chip-label">دسته بندی</InputLabel>
                            <Select
                              labelId="demo-multiple-chip-label"
                              id="demo-multiple-chip"
                              value={categories}
                              onChange={handleCategoryChange}
                              input={<OutlinedInput id="select-multiple-chip" label="Categories" />}
                              renderValue={(selected) => (
                                <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                                  {selected.map((value) => (
                                    <Chip
                                      key={value}
                                      label={
                                        categoriesList.filter(
                                          (a) => `${a._id}''` === `${value}''`
                                        )[0].text
                                      }
                                    />
                                  ))}
                                </Box>
                              )}
                              MenuProps={MenuProps}
                            >
                              {categoriesList &&
                                categoriesList.map((category) => (
                                  <MenuItem
                                    key={category._id}
                                    value={category._id}
                                    title={category.text}
                                    style={{ paddingLeft: category.padding, color: category.color }}
                                  >
                                    {category.text}
                                  </MenuItem>
                                ))}
                            </Select>
                          </FormControl>
                        </Grid>
                      )}

                      <Grid item lg={6} md={6} xs={12}>
                        <TextField
                          type="text"
                          label="جست و جو ..."
                          variant="outlined"
                          fullWidth
                          value={`${text}`}
                          onChange={(event) => {
                            setText(event.target.value);

                            // if (event.target.value.length > 3) {
                            // getData(event.target.value, category);
                            // }
                          }}
                          onKeyUp={(event) => {
                            if (event.key === "Enter") {
                              getData(event.target.value, category);
                            }
                          }}
                          InputProps={{
                            endAdornment: (
                              <IconButton
                                aria-label="search"
                                onClick={() => {
                                  getData(text, category);
                                }}
                                color="primary"
                              >
                                <ManageSearch position="end" fontSize="22" />
                              </IconButton>
                            ),
                          }}
                        />
                      </Grid>
                    </Grid>
                  </MDBox>
                  <Grid
                    container
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-around",
                    }}
                  >
                    {images &&
                      images.map((image) => (
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            textAlign: "center",
                            alignItems: "center",
                            border: "1px solid #000",
                            borderRadius: 5,
                            maxWidth: 300,
                            maxHeight: 300,
                            minWidth: 300,
                            minHeight: 300,
                            margin: 10,
                          }}
                        >
                          <div
                            style={{
                              width: 240,
                              minWidth: 240,
                              maxHeight: 240,
                              minHeight: 240,
                              resize: "center",
                              marginTop: 10,
                              marginBottom: 5,
                            }}
                          >
                            <ImageViewer>
                              <img
                                src={image.url}
                                alt="گالری محتشم"
                                style={{
                                  width: "100%",
                                  resize: "center",
                                }}
                              />
                            </ImageViewer>
                          </div>
                          <strong>{image.text}</strong>
                        </div>
                      ))}
                  </Grid>
                </MDBox>
              </LoadingOverlay>
            </MDBox>
          </MDBox>
        </Card>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}

export default catalogueView;
