// react-router-dom components
import { useNavigate, useParams } from "react-router-dom";

// @mui material components
import Card from "@mui/material/Card";

// Market Place React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import Swal from "sweetalert2";
import LoadingOverlay from "react-loading-overlay";

// Authentication layout components
// import Joi from "joi";
import { useEffect, useState } from "react";

import DashboardLayout from "component/LayoutContainers/DashboardLayout";
import DashboardNavbar from "component/Navbars/DashboardNavbar";
import { setDirection, useMaterialUIController } from "context";
import { Grid, TextField } from "@mui/material";
import Footer from "component/Footer"; 

const allowedRoles = ["supperadmin", "web"];
function Category() {
  const [text, setText] = useState("");
  const [loading, setLoading] = useState(false);
  const [controller, dispatch] = useMaterialUIController();
  const { id } = useParams();
  const navigate = useNavigate();
  const getData = async () => {
    setLoading(true);
    const res = {
      success:true,
      data:[]
    };
    setLoading(false);
    if (res.success) {
      setText(res.data.text);
    } else {
      Swal.fire({
        text: `${res.message} (${res.code})`,
        icon: "error",
        confirmButtonText: "OK",
      });
    }
    if (res.code === 401) {
      navigate("/", { replace: true });
    }
  };
  const handleSubmitClick = async () => {
    setLoading(true);
    const newUser = {
      text,
    };
    let res;
    if (id) {
      newUser.id = id;
      res ={
        success:true,
        data:[]
      };
    } else {
      res = {
        success:true,
        data:[]
      };
    }
    setLoading(false);
    if (res.success) {
      Swal.fire({
        html: res.message,
        icon: "success",
        confirmButtonText: "Ok",
      });
      navigate("/categories", { replace: true });
      // clearForm();
    } else {
      Swal.fire({
        text: `${res.message} (${res.code})`,
        icon: "error",
        confirmButtonText: "OK",
      });
    }
    if (res.code === 401) {
      navigate("/", { replace: true });
    }
  };
  const handleDeleteClick = async () => {
    Swal.fire({
      title: "",
      icon: "warning",
      text: "آیا مایل به حذف  هستید",
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: "بله",
      cancelButtonText: "خیر",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const res = {
          success:true,
          data:[]
        };
        if (res.success) {
          Swal.fire({
            text: res.message,
            icon: "success",
            confirmButtonText: "بسیار خوب",
          });
          if (controller.authenticated) {
            navigate("/categories", { replace: true });
          } else {
            navigate("/");
          }
        } else {
          Swal.fire({
            text: res.message,
            icon: "error",
            confirmButtonText: "بسیار خوب",
          });
        }
        if (res.errorcode === 401) {
          navigate("/", { replace: true });
        }
      }
    });
  };
  useEffect(() => {
     
    setDirection(dispatch, "rtl");
    if (id) {
      getData();
    }
  }, []);

  return (
    <DashboardLayout>
      <DashboardNavbar title="دسته بندی کاتالوگ" />
      <MDBox pt={6} pb={3}>
        <Card>
          <MDBox
            variant="gradient"
            bgColor="info"
            borderRadius="lg"
            coloredShadow="success"
            mx={2}
            mt={-3}
            p={1}
            mb={1}
            textAlign="center"
          >
            <MDTypography variant="h6" fontWeight="medium" color="white">
              دسته بندی کاتالوگ
            </MDTypography>
          </MDBox>
          <MDBox pt={4} pb={3} px={3}>
            <MDBox component="form" role="form">
              <LoadingOverlay
                className="overlayclass"
                active={loading}
                spinner
                text=" لطفا کمی صبر کنید"
              >
                <MDBox mb={2}>
                  <TextField
                    type="text"
                    label="دسته بندی"
                    variant="outlined"
                    fullWidth
                    value={`${text}`}
                    onChange={(event) => {
                      setText(event.target.value);
                    }}
                  />
                </MDBox>

                <Grid container spaceing={2} mt={4} mb={1} style={{ justifyContent: "flex-end" }}>
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={() => {
                      navigate("/categories");
                    }}
                  >
                    انصراف
                  </MDButton>
                  {id && (
                    <MDButton
                      style={{ marginRight: 8, marginLeft: 8 }}
                      variant="gradient"
                      color="error"
                      onClick={() => {
                        handleDeleteClick();
                      }}
                    >
                      حذف
                    </MDButton>
                  )}
                  <MDButton
                    style={{ marginRight: 8, marginLeft: 8 }}
                    variant="gradient"
                    color="info"
                    onClick={() => {
                      handleSubmitClick();
                    }}
                  >
                    {id ? "ویرایش" : "ثبت"}
                  </MDButton>
                </Grid>
              </LoadingOverlay>
            </MDBox>
          </MDBox>
        </Card>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Category;
