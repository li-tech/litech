import { useEffect, useState } from "react";
// @mui material components
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
// Acromegaly React components
import MDBox from "components/MDBox";
import Swal from "sweetalert2";
import LoadingOverlay from "react-loading-overlay";
// Acromegaly React example components
import DashboardLayout from "component/LayoutContainers/DashboardLayout";
import DashboardNavbar from "component/Navbars/DashboardNavbar";
import PropTypes from "prop-types";
// Acromegaly React contexts
import { useMaterialUIController, setDirection } from "context";
import { useNavigate, useParams } from "react-router-dom";
import { Box, TextField } from "@mui/material";
import MDButton from "components/MDButton";
import MDTypography from "components/MDTypography";
import Footer from "component/Footer"; 

function TablePaginationActions(props) {
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5, direction: "rtl" }}>
      <IconButton onClick={handleFirstPageButtonClick} disabled={page === 0} aria-label="صفحه اول">
        <LastPageIcon />
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        <KeyboardArrowRight />
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="اخرین صفحه"
      >
        <KeyboardArrowLeft />
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="صفحه قبلی"
      >
        <FirstPageIcon />
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const filters = {
  userRole: "",
};

const allowedRoles = ["supperadmin", "web"];
function Categories() {
  const [controller, dispatch] = useMaterialUIController();
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(0);
  const [filter, setFilter] = useState(filters);
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(0);
  const [datas, setDatas] = useState([]);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const { role } = useParams();
  const navigate = useNavigate();
  let emptyRows = rowsPerPage - Math.min(rowsPerPage, count - page * rowsPerPage);

  // Avoid a layout jump when reaching the last page with empty rows.
  const getData = async (perPagex, pagex, filterx, searchx) => {
    setLoading(true);
    setDatas([]);
    const newDatas = { perPage: perPagex, page: pagex, search: searchx };
    const data = {
      success:true,
      data:[]
    };
    if (data.success) {
      setDatas(data.data);

      setCount(data.count);
      emptyRows = rowsPerPage - Math.min(rowsPerPage, datas.length - (pagex - 1) * rowsPerPage);
    } else {
      Swal.fire({
        text: data.message,
        icon: "error",
        confirmButtonText: "بسیار خوب",
      });

      if (data.code === 401) {
        navigate("/", { replace: true });
      }
    }

    setLoading(false);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    getData(rowsPerPage, newPage + 1, filter, search);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setFilter(filters);
    getData(parseInt(event.target.value, 10), 1, filter, search);
  };

  useEffect(() => {
     
    setDirection(dispatch, "rtl");
    const temp = filter;
    if (role) {
      temp.userRole = role;
    } else {
      temp.userRole = "";
    }
    setFilter(temp);
    getData(10, 1, temp, "");
    return () => {
      setFilter();
    };
  }, []);

  return (
    <DashboardLayout>
      <DashboardNavbar title=" دسته بندی ها" />
      <MDBox pt={6} pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <Card>
              <MDBox
                mx={2}
                mt={-3}
                py={1}
                px={2}
                variant="gradient"
                bgColor="info"
                borderRadius="lg"
                coloredShadow="info"
              >
                <Grid container>
                  <Grid item xs={6} sm={6}>
                    <MDTypography variant="h6" fontWeight="medium" color="white">
                      دسته بندی ها{" "}
                    </MDTypography>
                  </Grid>
                  <Grid item xs={6} sm={6} style={{ justifyContent: "flex-end" }}>
                    <MDButton
                      style={{ float: "left" }}
                      variant="contained"
                      color="primary"
                      onClick={() => {
                        navigate("/category");
                      }}
                    >
                      ثبت جدید
                    </MDButton>
                  </Grid>
                </Grid>
              </MDBox>
              <MDBox pt={3}>
                <LoadingOverlay
                  className="overlayclass"
                  active={loading}
                  spinner
                  text="لطفا کمی صبر کنید"
                >
                  <Grid container spacing={2} mr={1} ml={1} mb={2}>
                    <Grid item lg={4} md={4} xs={5}>
                      <TextField
                        autoFocus
                        id="search"
                        fullWidth
                        variant="outlined"
                        value={search}
                        placeholder="جستجو..."
                        onChange={(event) => {
                          setSearch(event.target.value);
                          if (event.target.value === "") {
                            setPage(0);
                            getData(rowsPerPage, 1, filter, "");
                          }
                        }}
                        onKeyPress={(ev) => {
                          if (ev.key === "Enter") {
                            setPage(0);
                            getData(rowsPerPage, 1, filter, search);
                          }
                        }}
                      />
                    </Grid>
                  </Grid>
                  <TableContainer component={Paper} style={{ borderWidth: 1, borderColor: "#000" }}>
                    <Table>
                      <TableBody>
                        {/* <TableRow style={{ backgroundColor: "#ccc" }}>
                          <TableCell>
                          </TableCell>
                        </TableRow> */}
                        {datas.map((row) => (
                          <TableRow
                            sx={{ "& > *": { borderBottom: "unset" } }}
                            key={row.name}
                            style={{
                              borderWidth: 1,
                              borderColor: "#000",
                              borderRadius: 5,
                            }}
                          >
                            <TableCell>
                              <div className="borderdtbltr">
                                <Grid className="tblhrader" container px={1}>
                                  <Grid
                                    container
                                    lg={11}
                                    md={10}
                                    xs={10}
                                    onClick={() => {
                                      navigate(`/category/${row._id}`);
                                    }}
                                  >
                                    <Grid
                                      item
                                      lg={12}
                                      md={12}
                                      xs={12}
                                      style={{ textAlign: "right" }}
                                    >
                                      <b> عنوان: </b>
                                      {row.text}
                                    </Grid>
                                  </Grid>
                                </Grid>
                              </div>
                            </TableCell>
                          </TableRow>
                        ))}
                        {emptyRows > 0 && (
                          <TableRow style={{ height: 0 }}>
                            <TableCell />
                          </TableRow>
                        )}
                      </TableBody>
                      <TableFooter>
                        <TableRow>
                          <TablePagination
                            rowsPerPageOptions={[5, 10, 25, { label: "همه", value: -1 }]}
                            count={count}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                              inputProps: {
                                "aria-label": "سطر در هر صفحه",
                              },
                              native: true,
                            }}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActions}
                          />
                        </TableRow>
                      </TableFooter>
                    </Table>
                  </TableContainer>
                </LoadingOverlay>
              </MDBox>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Categories;
