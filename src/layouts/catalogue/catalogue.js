// react-router-dom components
import { useNavigate, useParams } from "react-router-dom";

// @mui material components
import Card from "@mui/material/Card";

// Market Place React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import Swal from "sweetalert2";
import LoadingOverlay from "react-loading-overlay";

// Authentication layout components
// import Joi from "joi";
import { useEffect, useState } from "react";

import DashboardLayout from "component/LayoutContainers/DashboardLayout";
import DashboardNavbar from "component/Navbars/DashboardNavbar";
import { setDirection, useMaterialUIController } from "context";
import {
  Box,
  Chip,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Modal,
  OutlinedInput,
  Select,
  TextField,
} from "@mui/material";
import Footer from "component/Footer"; 
import config from "config";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
const allowedRoles = ["supperadmin", "web"];
function Catalogue() {
  const [picture, setPicture] = useState();
  const [text, setText] = useState("");
  const [loading, setLoading] = useState(false);
  const [selectedImage, setSelectedImage] = useState();
  const [openImage, setOpenImage] = useState(false);
  const [categories, setCategories] = useState([]);
  const [categoriesList, setCategoriesList] = useState();

  const [controller, dispatch] = useMaterialUIController();
  const { id } = useParams();
  const navigate = useNavigate();
  const getData = async () => {
    setLoading(true);
    const newDatas = { perPage: 10000, page: 1, search: "" };
    const resList = {
      success:true,
      data:[]
    };
    setLoading(false);
    if (resList.success) {
      setCategoriesList(resList.data);
    } else {
      Swal.fire({
        text: `${resList.message} (${resList.code})`,
        icon: "error",
        confirmButtonText: "OK",
      });
    }
    if (resList.code === 401) {
      navigate("/", { replace: true });
    }
    if (id) {
      const res = {
        success:true,
        data:[]
      };

      if (res.success) {
        setText(res.data.text);
        setPicture(res.data.picture);
        setCategories(res.data.categories);
      } else {
        Swal.fire({
          text: `${res.message} (${res.code})`,
          icon: "error",
          confirmButtonText: "OK",
        });
      }
      if (res.code === 401) {
        navigate("/", { replace: true });
      }
    }
  };
  const handleCategoryChange = (event) => {
    const {
      target: { value },
    } = event;
    const temp = typeof value === "string" ? value.split(",") : value;
    setCategories(temp);
  };
  const handleSubmitClick = async () => {
    if (!selectedImage && !picture) {
      Swal.fire({
        text: `لطفا یک تصویر برای کاتالوگ انتخاب کنید`,
        icon: "error",
        confirmButtonText: "OK",
      });
      return;
    }
    setLoading(true);
    const newUser = {
      text,
      picture,
      categories,
    };
    let res;
    if (id) {
      newUser.id = id;
      res = {
        success:true,
        data:[]
      };
    } else {
      res = {
        success:true,
        data:[]
      };
    }
    setLoading(false);
    if (res.success) {
      Swal.fire({
        html: res.message,
        icon: "success",
        confirmButtonText: "Ok",
      });
      navigate("/Catalogues", { replace: true });
      // clearForm();
    } else {
      Swal.fire({
        text: `${res.message} (${res.code})`,
        icon: "error",
        confirmButtonText: "OK",
      });
    }
    if (res.code === 401) {
      navigate("/", { replace: true });
    }
  };
  const changeHandler = (event) => {
    if (event.target.files[0].size > 307200) {
      Swal.fire({
        text: `حجم تصویر نمی تواند بیشتر از ۳۰۰ کلیوبایت باشد`,
        icon: "error",
        confirmButtonText: "متوجه شدم",
      });
      return;
    }
    setSelectedImage(event.target.files[0]);
  };
  const handleDeleteClick = async () => {
    Swal.fire({
      title: "",
      icon: "warning",
      text: "آیا مایل به حذف  هستید",
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: "بله",
      cancelButtonText: "خیر",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const res = {
          success:true,
          data:[]
        };
        if (res.success) {
          Swal.fire({
            text: res.message,
            icon: "success",
            confirmButtonText: "بسیار خوب",
          });
          if (controller.authenticated) {
            navigate("/Catalogues", { replace: true });
          } else {
            navigate("/");
          }
        } else {
          Swal.fire({
            text: res.message,
            icon: "error",
            confirmButtonText: "بسیار خوب",
          });
        }
        if (res.errorcode === 401) {
          navigate("/", { replace: true });
        }
      }
    });
  };
  const handleClose = () => {
    setOpenImage(false);
  };
  useEffect(() => {
   
    setDirection(dispatch, "rtl");

    getData();
  }, []);

  return (
    <DashboardLayout>
      <DashboardNavbar title="کاتالوگ" />
      <MDBox pt={6} pb={3}>
        <Card>
          <MDBox
            variant="gradient"
            bgColor="info"
            borderRadius="lg"
            coloredShadow="success"
            mx={2}
            mt={-3}
            p={1}
            mb={1}
            textAlign="center"
          >
            <MDTypography variant="h6" fontWeight="medium" color="white">
              کاتالوگ
            </MDTypography>
          </MDBox>
          <MDBox pt={4} pb={3} px={3}>
            <MDBox component="form" role="form">
              <LoadingOverlay className="overlayclass" active={loading} spinner text=" لطفا کمی صبر کنید">
                <MDBox mb={2}>
                  <Grid container spacing={2} mb={1}>
                    <Grid item lg={4} md={6} xs={8}>
                      <div className="custom-file">
                        <input
                          type="file"
                          accept=".png,.jpeg,.jpg"
                          className="custom-file-input"
                          id="image"
                          name="image"
                          onChange={changeHandler}
                        />
                        <div id="imageStatus" className="custom-file-label" htmlFor="selectedImage">
                          {selectedImage && selectedImage.name}
                          {!selectedImage && "انتخاب تصویر"}
                        </div>
                      </div>
                    </Grid>
                    <Grid
                      item
                      lg={4}
                      md={6}
                      xs={4}
                      onClick={() => {
                        setOpenImage(true);
                      }}
                    >
                      {picture && (
                        <img
                          src={`${config.fileserver}catalog/${picture}`}
                          style={{ maxWidth: 75, width: "100%", resize: "center" }}
                            alt="گالری محتشم"
                        />
                      )}
                    </Grid>
                  </Grid>
                  <Grid container spacing={2} mb={1}>
                    {categoriesList && (
                      <Grid item lg={12} md={12} xs={12}>
                        <FormControl fullWidth>
                          <InputLabel id="demo-multiple-chip-label">دسته بندی</InputLabel>
                          <Select
                            labelId="demo-multiple-chip-label"
                            id="demo-multiple-chip"
                            multiple
                            value={categories}
                            onChange={handleCategoryChange}
                            input={<OutlinedInput id="select-multiple-chip" label="Categories" />}
                            renderValue={(selected) => (
                              <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                                {selected.map((value) => (
                                  <Chip
                                    key={value}
                                    label={
                                      categoriesList.filter((a) => `${a._id}''` === `${value}''`)[0]
                                        .text
                                    }
                                  />
                                ))}
                              </Box>
                            )}
                            MenuProps={MenuProps}
                          >
                            {categoriesList &&
                              categoriesList.map((category) => (
                                <MenuItem
                                  key={category._id}
                                  value={category._id}
                                  title={category.text}
                                  style={{ paddingLeft: category.padding, color: category.color }}
                                >
                                  {category.text}
                                </MenuItem>
                              ))}
                          </Select>
                        </FormControl>
                      </Grid>
                    )}
                  </Grid>
                  <TextField
                    type="text"
                    label="عنوان"
                    variant="outlined"
                    fullWidth
                    value={`${text}`}
                    onChange={(event) => {
                      setText(event.target.value);
                    }}
                  />
                </MDBox>

                <Grid container spaceing={2} mt={4} mb={1} style={{ justifyContent: "flex-end" }}>
                  <MDButton
                    variant="gradient"
                    color="info"
                    onClick={() => {
                      navigate("/Catalogues");
                    }}
                  >
                    انصراف
                  </MDButton>
                  {id && (
                    <MDButton
                      style={{ marginRight: 8, marginLeft: 8 }}
                      variant="gradient"
                      color="error"
                      onClick={() => {
                        handleDeleteClick();
                      }}
                    >
                      حذف
                    </MDButton>
                  )}
                  <MDButton
                    style={{ marginRight: 8, marginLeft: 8 }}
                    variant="gradient"
                    color="info"
                    onClick={() => {
                      handleSubmitClick();
                    }}
                  >
                    {id ? "ویرایش" : "ثبت"}
                  </MDButton>
                </Grid>
              </LoadingOverlay>
            </MDBox>
          </MDBox>
        </Card>
      </MDBox>
      <Modal
        open={openImage}
        onClose={handleClose}
        style={{
          width: "90%",
          maxWidth: 750,
          maxHeight: 750,
          position: "absolute",
          top: 25,
          left: "auto",
          right: "5%",
        }}
      >
        <Grid onClick={handleClose}>
          {picture && (
            <img
              src={`${config.fileserver}catalog/${picture}`}
              style={{ width: "100%", resize: "center" }}
                alt="گالری محتشم"
            />
          )}
        </Grid>
      </Modal>
      <Footer />
    </DashboardLayout>
  );
}

export default Catalogue;
