import { createContext, useContext, useReducer, useMemo } from "react";

// prop-types is a library for typechecking of props
import PropTypes from "prop-types";

// mohtasham React main context
const MaterialUI = createContext();

// Setting custom name for the context which is visible on react dev tools
MaterialUI.displayName = "Li-tech";

// mohtasham React reducer
function reducer(state, action) {
  try {
    switch (action.type) {
      case "MINI_SIDENAV": {
        return { ...state, miniSidenav: action.value };
      }
      case "TRANSPARENT_SIDENAV": {
        return { ...state, transparentSidenav: action.value };
      }
      case "WHITE_SIDENAV": {
        return { ...state, whiteSidenav: action.value };
      }
      case "SIDENAV_COLOR": {
        return { ...state, sidenavColor: action.value };
      }
      case "TRANSPARENT_NAVBAR": {
        return { ...state, transparentNavbar: action.value };
      }
      case "FIXED_NAVBAR": {
        return { ...state, fixedNavbar: action.value };
      }
      case "OPEN_CONFIGURATOR": {
        return { ...state, openConfigurator: action.value };
      }
      case "DIRECTION": {
        return { ...state, direction: action.value };
      }
      case "LAYOUT": {
        return { ...state, layout: action.value };
      }
      case "DARKMODE": {
        return { ...state, darkMode: action.value };
      }
      case "login": {
        const newData = {
          miniSidenav: false,
          transparentSidenav: false,
          whiteSidenav: false,
          sidenavColor: "info",
          transparentNavbar: true,
          fixedNavbar: true,
          openConfigurator: false,
          direction: "rtl",
          layout: "dashboard",
          darkMode: false,
          token: action.value.token,
          userRole: action.value.userRole,
          id: action.value.id,
          authenticated: true,
          name: action.value.name,
        };
        localStorage.setItem("@user", JSON.stringify(newData));
        return newData;
      }
      case "logout": {
        localStorage.setItem(
          "@user",
          JSON.stringify({
            miniSidenav: true,
            transparentSidenav: false,
            whiteSidenav: false,
            sidenavColor: "info",
            transparentNavbar: true,
            fixedNavbar: true,
            openConfigurator: false,
            direction: "rtl",
            darkMode: false,
            token: "",
            userRole: "",
            name: "",
            id: "",
            authenticated: false,
          })
        );
        const newData = {
          miniSidenav: true,
          transparentSidenav: false,
          whiteSidenav: false,
          sidenavColor: "info",
          transparentNavbar: true,
          fixedNavbar: true,
          openConfigurator: false,
          direction: "rtl",
          layout: "dashboard",
          darkMode: false,
          token: "",
          userRole: "",
          id: "",
          authenticated: false,
          name: "",
        };

        return newData;
      }
      default: {
        throw new Error(`Unhandled action type: ${action.type}`);
      }
    }
  } catch (err) {
    console.log(err);
    return state;
  }
}
let initialState = localStorage.getItem("@user");

if (!initialState) {
  initialState = {
    miniSidenav: true,
    transparentSidenav: false,
    whiteSidenav: false,
    sidenavColor: "info",
    transparentNavbar: true,
    fixedNavbar: true,
    openConfigurator: false,
    direction: "rtl",
    layout: "page",
    darkMode: false,
    token: "",
    userRole: "",
    name: "",
    authenticated: false,
  };
  localStorage.setItem("@user", JSON.stringify(initialState));
} else {
  initialState = JSON.parse(initialState);
}

// mohtasham React context provider
function MaterialUIControllerProvider({ children }) {
  // const applicationData = await localStorage.getItem("@user");
 
  const [controller, dispatch] = useReducer(reducer, initialState);

  const value = useMemo(() => [controller, dispatch], [controller, dispatch]);

  return <MaterialUI.Provider value={value}>{children}</MaterialUI.Provider>;
}

// mohtasham React custom hook for using context
function useMaterialUIController() {
  const context = useContext(MaterialUI);

  if (!context) {
    throw new Error("use Controller should be used inside the  ControllerProvider.");
  }

  return context;
}

// Typechecking props for the  ControllerProvider
MaterialUIControllerProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

// Context module functions
const setMiniSidenav = (dispatch, value) => dispatch({ type: "MINI_SIDENAV", value });
const setTransparentSidenav = (dispatch, value) => dispatch({ type: "TRANSPARENT_SIDENAV", value });
const setWhiteSidenav = (dispatch, value) => dispatch({ type: "WHITE_SIDENAV", value });
const setSidenavColor = (dispatch, value) => dispatch({ type: "SIDENAV_COLOR", value });
const setTransparentNavbar = (dispatch, value) => dispatch({ type: "TRANSPARENT_NAVBAR", value });
const setFixedNavbar = (dispatch, value) => dispatch({ type: "FIXED_NAVBAR", value });
const setOpenConfigurator = (dispatch, value) => dispatch({ type: "OPEN_CONFIGURATOR", value });
const setDirection = (dispatch, value) => dispatch({ type: "DIRECTION", value });
const setLogin = (dispatch, value) => dispatch({ type: "login", value });
const setLayout = (dispatch, value) => dispatch({ type: "LAYOUT", value });
const setDarkMode = (dispatch, value) => dispatch({ type: "DARKMODE", value });

export {
  MaterialUIControllerProvider,
  useMaterialUIController,
  setMiniSidenav,
  setTransparentSidenav,
  setWhiteSidenav,
  setSidenavColor,
  setTransparentNavbar,
  setFixedNavbar,
  setOpenConfigurator,
  setDirection,
  setLayout,
  setDarkMode,
  setLogin,
};
